import { Component } from '@angular/core';

@Component({
  selector: 'app-banner',
  standalone: true,
  imports: [],
  templateUrl: './banner.component.html',
  styleUrl: './banner.component.css'
})
export class BannerComponent {
  title = 'Titre activité';
  description =
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet ducimus architecto laudantium, illo dolorum eius doloremque labore explicabo voluptatibus qui necessitatibus optio saepe nihil incidunt debitis minima perspiciatis facere voluptatum.';
  date = '2 mars - 6 mars';
  bannerImg = '../../../assets/banner3.jpg';
}
