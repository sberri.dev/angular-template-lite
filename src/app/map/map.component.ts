import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import * as L from 'leaflet';
import { EventService } from '../event.service';
import { Event } from '../event';
import { SearchBarPipe } from '../search-bar.pipe';

@Component({
  selector: 'app-map',
  standalone: true,
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent {

  constructor(
    private eventService: EventService, 
    private searchBarPipe: SearchBarPipe
    ){}

  @Input() selectedCategoryFilter?: string;
  @Input() selectedCityFilter?: string;
  @Input() selectedDateFilter?: Date;
  @Input() searchTerms: string = '';

  map!: L.Map;
  eventList: Event[] = [];
  markers: L.Marker[] = [];

  private initMap(): void {
    this.map = L.map('map', {
      center: [43.6077894, 1.44103072 ],
      zoom: 11
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }

  ngOnInit() {
    this.eventService.getEvents().then(events => {
      console.log(events)
      this.eventList = events.results;
      this.initMap();
      this.createMarkers();
      this.filterMarkers();
    })
  }

  addMarkerForEvent(event: Event) {
    if (event.geo_point) {
      const lat = event.geo_point.lat;
      const lon = event.geo_point.lon;
  
      const marker = L.marker([lat, lon]);
      marker.bindPopup(
        `
        <main class="home-activityCard">
          <p class="tag">${event.categorie_de_la_manifestation}</p>
          <div class="home-activityCard-allDetails">
            <h2>${event.nom_de_la_manifestation}</h2>
            <p>${event.descriptif_court}</p>
            <section class="home-activityCard-details">
              <span class="detail1">${event.date_debut}</span>
            </section>
          </div>
        </main>
        `
      );

      // Ajoute le marker à la map
      marker.addTo(this.map);

      // Ajoute le marker au tableau des markers. Ce tableau garde une référence de tous les marqueurs affichés à l'instant T
      // Il est utilisé pour savoir quels markers sont conservés après filtrage
      // Avant chaque filtrage, on vide le tableau (this.markers.forEach(marker => marker.remove()) pour le remplir avec ceux qui
      // répondent aux conditions de filtrage
      this.markers.push(marker);
    }
  }
  
  createMarkers() {
    this.eventList.forEach(event => {
      this.addMarkerForEvent(event);
    });
  }
  
  filterMarkers(): void {
    // Suppression des marqueurs existants
    this.markers.forEach(marker => marker.remove());

    console.log("REMOVE", this.markers);


    // Applique le filtre de recherche à l'aide du pipe
    let filteredEvents = this.searchBarPipe.transform(this.eventList, this.searchTerms);

    console.log("Résultats du Pipe:", filteredEvents);    
    if (this.selectedCategoryFilter) {
      filteredEvents = filteredEvents.filter(event => event.categorie_de_la_manifestation === this.selectedCategoryFilter);
    }
    if (this.selectedCityFilter) {
      filteredEvents = filteredEvents.filter(event => event.commune === this.selectedCityFilter);
    }
    if (this.selectedDateFilter) {
      filteredEvents = filteredEvents.filter(event => event.date_debut === this.selectedDateFilter);
    }

    console.log("FILTEREDEVENTS", filteredEvents)

    // Ajoute les marqueurs pour les événements filtrés
    filteredEvents.forEach(event => {
      this.addMarkerForEvent(event);
    });
  }
  
  ngOnChanges(changes: SimpleChanges) {
    if (changes['selectedCityFilter'] || changes['selectedDateFilter'] || changes['selectedCategoryFilter'] || changes['searchTerms']) {
      console.log("MAP", changes); 
      this.filterMarkers();
    }
  }  
}