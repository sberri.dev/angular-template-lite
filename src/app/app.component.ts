import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HeaderComponent } from './shared/header/header.component';
import { ButtonComponent } from './shared/buttons/button/button.component';
import { EventCardComponent } from './shared/event-card/event-card.component';
import { FooterComponent } from './shared/footer/footer.component';

@Component({
  selector: 'app-root',
  standalone: true,
  // Importation de tous les composants
  imports: [
    RouterOutlet,
    HeaderComponent,
    ButtonComponent,
    EventCardComponent,
    FooterComponent
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'angular-template-lite'; //
}
