import { Routes } from '@angular/router';
import { HomeComponent } from './home/home/home.component';
import { DetailsEventPageComponent } from './details-event-page/details-event-page.component';
import { AllEventsComponent } from './all-events/all-events.component';

// Définition des routes du site
export const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'event-details', component: DetailsEventPageComponent },
    {path: 'events', component: AllEventsComponent}, 
];
