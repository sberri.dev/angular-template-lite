import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsEventPageComponent } from './details-event-page.component';

describe('DetailsEventPageComponent', () => {
  let component: DetailsEventPageComponent;
  let fixture: ComponentFixture<DetailsEventPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DetailsEventPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DetailsEventPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
