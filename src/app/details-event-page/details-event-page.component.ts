import { Component } from '@angular/core';
import { ButtonComponent } from '../shared/buttons/button/button.component';
@Component({
  selector: 'app-details-event-page',
  standalone: true,
  imports: [ButtonComponent],
  templateUrl: './details-event-page.component.html',
  styleUrl: './details-event-page.component.css'
})
export class DetailsEventPageComponent {

}
